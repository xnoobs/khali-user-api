# README #

### Cloning
```git clone --recursive https://bitbucket.org/durdanto-rafi/khali-user-api.git```   
If unfortunately forgot to add --recursive , then you can run ```git submodule update --init --recursive``` this command and everything will be fine.  

To Update Submodule in latest commits:  
```git submodule update```

#### Special Note
If you create a pull request from submodule:  
1. First merge the submodule pull request.  
2. Then check out submodule in latest develop/master branch  
3. Then again push submodule update change from root module  
4. Then merge the root module  

#### Mongo db in local
Go to root of the project. Then run the following command:  
```docker-compose up -d```

#### To run/build the project
Got to your project root directory. Then run the following command:   
```./gradlew application:bootRun``` 

To build:  
```./gradlew application:build``` 

#### Intellij Idea setup
1. File ->  New -> Project From existing sources -> build.gradle click
2. File -> Settings -> plugins -> lombok (search) and install
3. File -> Settings -> plugins -> google-java-format (search) and install
4. File -> Settings -> plugins -> sava action (search) and install 
5. File -> Settings -> Build, Execution, Deployment -> Compiler -> Annotation Processor and enable it
6. File -> Settings -> Other Settings -> google-java-formatter and enable it
7. File -> Settings -> Other Settings -> save actions -> General(Check first three), Formatting Action (Check Reformat file)

#### bootRun in Intellij Idea
Select Gradle icon in the right side of IDE. khali-user-api -> application -> Tasks -> application -> bootRun (Double click).
At first it will fail. There is run icon in the top right side. In the left side of it click arrow down and select Edit Configuration. 
In Gradle project -> type (khali-user-api) only. 
In Tasks -> type application:bootRun 
Then click run button.  

#### Hot reload in Intellij idea
Open the Settings --> Build-Execution-Deployment --> Compiler and enable :
```Build Project Automatically.```

press ctrl+shift+A and search for the registry. In the registry, enable :
```compiler.automake.allow.when.app.running```
OKKKK





