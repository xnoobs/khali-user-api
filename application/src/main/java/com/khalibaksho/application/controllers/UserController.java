package com.khalibaksho.application.controllers;

import com.khalibaksho.application.services.UserService;
import com.khalibaksho.commonmodule.models.userapi.request.UserForm;
import com.khalibaksho.commonmodule.models.userapi.response.UserListResponse;
import com.khalibaksho.commonmodule.models.userapi.response.UserResponse;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

/**
 * User controller
 *
 * @author sarouarhossain
 */
@RestController
@RequestMapping("/user")
public class UserController {
  private final UserService userService;

  UserController(UserService userService) {
    this.userService = userService;
  }

  /**
   * Api to get all users
   *
   * @return user list model
   */
  @GetMapping("")
  public Mono<UserListResponse> get() {
    return userService.getAllUsers();
  }

  /**
   * Register a user in DB
   *
   * @param userForm request body form
   * @return created user
   */
  @PostMapping("")
  public Mono<UserResponse> post(@RequestBody UserForm userForm) {
    return userService.register(userForm);
  }
}
