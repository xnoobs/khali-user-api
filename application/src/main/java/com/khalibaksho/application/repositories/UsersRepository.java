package com.khalibaksho.application.repositories;

import com.khalibaksho.application.models.Users;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;

public interface UsersRepository extends ReactiveMongoRepository<Users, String> {
}
