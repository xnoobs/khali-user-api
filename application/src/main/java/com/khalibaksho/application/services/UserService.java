package com.khalibaksho.application.services;

import com.khalibaksho.application.models.Users;
import com.khalibaksho.commonmodule.models.userapi.request.UserForm;
import com.khalibaksho.commonmodule.models.userapi.response.UserListResponse;
import com.khalibaksho.commonmodule.models.userapi.response.UserResponse;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * User service interface ....
 *
 * @author sarouarhossain
 */
public interface UserService {
  /**
   * To insert user in database
   *
   * @param userForm request form
   * @return inserted user info
   */
  Mono<UserResponse> register(final UserForm userForm);

  /**
   * Get all registered users
   *
   * @return user list response
   */
  Mono<UserListResponse> getAllUsers();
}
