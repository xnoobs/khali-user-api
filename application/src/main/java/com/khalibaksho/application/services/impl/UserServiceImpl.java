package com.khalibaksho.application.services.impl;

import com.khalibaksho.application.models.Users;
import com.khalibaksho.application.repositories.UsersRepository;
import com.khalibaksho.application.services.UserService;
import com.khalibaksho.commonmodule.models.userapi.request.UserForm;
import com.khalibaksho.commonmodule.models.userapi.response.UserListResponse;
import com.khalibaksho.commonmodule.models.userapi.response.UserResponse;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

/**
 * Implementation class of UserService interface
 *
 * @author sarouarhossain
 */
@Service
public class UserServiceImpl implements UserService {
  private final UsersRepository usersRepository;

  UserServiceImpl(UsersRepository usersRepository) {
    this.usersRepository = usersRepository;
  }

  @Override
  public Mono<UserResponse> register(UserForm userForm) {
    Users users = new Users();
    users.setName(userForm.getName());
    users.setEmail(userForm.getEmail());
    users.setPassword(userForm.getPassword());
    return usersRepository.save(users).map(this::convertUserEntityToResponse);
  }

  @Override
  public Mono<UserListResponse> getAllUsers() {
    return usersRepository
        .findAll()
        .map(this::convertUserEntityToResponse)
        .collectList()
        .map(userList -> UserListResponse.builder().users(userList).build());
  }

  /**
   * convert user entity to user response model
   *
   * @param users user entity
   * @return user response model
   */
  private UserResponse convertUserEntityToResponse(Users users) {
    return UserResponse.builder()
        .id(users.getId())
        .name(users.getName())
        .email(users.getEmail())
        .build();
  }
}
